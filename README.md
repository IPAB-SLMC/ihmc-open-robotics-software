This repository uses the git-flow branching model. You can find more about git-flow [here](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow).

## Licensing

Individual projects all have their own license information. We use a combination of Apache 2.0 and GPLv3. Consult the license file in each project for more information.
